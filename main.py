__author__ = 'merfie'
#import python classes
import time
import urllib
import urllib.request
import json
#import program classes
import bot
import config

def check_seconds_passed(seconds, timer):
    if time.time() - timer > seconds:
        return True
    else:
        return False


def check_user(user):
    url = 'https://api.twitch.tv/kraken/streams/' + user
    try:
        info = json.loads(urllib.request.urlopen(url, timeout=15).read().decode())
        if info["stream"] is None:
            return False
        else:
            return True
    except BaseException as e:
        print(e)

live = False
check_connectivity_time = time.time()
check_user_time = time.time()
check_ticket_time = time.time()

while True:
    #Check to see if the channel is live or offline every 15 seconds
    if check_seconds_passed(config.CHECK_ONLINE_TIME, check_connectivity_time):
        if check_user(config.CHANNEL):
            if not live:
                print(config.CHANNEL + " is live!")
                twitch_bot = bot.Bot()
                twitch_bot.send_pass(config.PASS)
                twitch_bot.send_nick(config.NICK)
                twitch_bot.join_channel(config.CHANNEL)
                twitch_bot.connect_to_db()
            live = True
        else:
            if live:
                print(config.CHANNEL + " has gone offline!")
                twitch_bot.part_channel(config.CHANNEL)
                twitch_bot.disconnect_to_db()
            live = False
        check_connectivity_time = time.time()

    #If we are live we need to process new messages
    if live is True:
        if check_seconds_passed(config.CHECK_USER_TIME, check_user_time):
            twitch_bot.update_viewer_list(config.CHANNEL)
            twitch_bot.award_user_time()
            check_user_time = time.time()
        if check_seconds_passed(config.CHECK_TICKET_TIME, check_ticket_time):
            twitch_bot.award_ticket()
            check_ticket_time = time.time()
        twitch_bot.receive_messages()

    #Sleep for a tenth of a second to give the CPU time to rest
    time.sleep(config.SLEEP_TIME)
