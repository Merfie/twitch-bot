__author__ = 'lawsean'
import time


class User:

    #seconds between ticket awardings
    TICKET_INTERVAL = 1200

    def __init__(self, username, cursor):
        self.username = username
        self.lastActive = time.time()
        self.totalMessages = 0
        self.totalSwears = 0
        self.timeActive = 0
        self.active = True
        query = """UPDATE twitchstats_userstats SET streams_viewed = streams_viewed + 1 WHERE username=%s;
                    INSERT INTO twitchstats_userstats (username, streams_viewed, messages_sent, minutes_watched, raffle_tickets)
                        SELECT %s, 1, 0, 0, 0
                        WHERE NOT EXISTS (SELECT 1 FROM twitchstats_userstats WHERE username=%s);"""
        data = (self.username, self.username, self.username)
        cursor.execute(query, data)

    def add_time(self, cursor):
        self.timeActive = time.time() - self.lastActive
        self.lastActive = time.time()
        query = """UPDATE twitchstats_userstats SET minutes_watched = minutes_watched + %s WHERE username=%s;"""
        data = (self.timeActive, self.username,)
        cursor.execute(query, data)

    def award_ticket(self, cursor):
        query = """UPDATE twitchstats_userstats SET raffle_tickets = raffle_tickets + 1 WHERE username=%s;"""
        data = (self.username,)
        cursor.execute(query, data)

    def add_message(self, cursor):
        self.totalMessages += 1
        query = """UPDATE twitchstats_userstats SET messages_sent = messages_sent + 1 WHERE username=%s;"""
        data = (self.username,)
        cursor.execute(query, data)
        print(self.username + " has sent " + str(self.totalMessages) + " messages.")
