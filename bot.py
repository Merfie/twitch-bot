__author__ = 'lawsean'
import socket
import re
import urllib
import urllib.request
import json
import psycopg2
import config
import user


class Bot:

    def __init__(self):
        self.con = socket.socket()
        self.con.connect((config.HOST, config.PORT))
        self.con.setblocking(0)
        self.data = ""
        self.mods_list = None
        self.viewers_list = {}
        self.conn_string = None
        self.conn = None
        self.cursor = None
        self.awarding_tickets = False

    def connect_to_db(self):
        self.conn_string = "host='" + config.IP_ADDRESS + "' dbname='" + config.DB_NAME + "' user='" + config.USER + "' password='" + config.PASSWORD + "'"
        self.conn = psycopg2.connect(self.conn_string)
        self.cursor = self.conn.cursor()
        print("CONNECTED TO DB")

    def disconnect_to_db(self):
        self.conn.close()
        self.viewers_list.clear()

    def send_pong(self, msg):
        self.con.send(bytes("PONG %s\r\n" % msg, 'UTF-8'))

    def send_private_message(self, chan, msg):
        self.con.send(bytes("PRIVMSG %s :%s\r\n" % (chan, msg), 'UTF-8'))

    def send_public_message(self, msg):
        self.con.send(bytes(msg, 'UTF-8'))

    def send_nick(self, nick):
        try:
            self.con.send(bytes("NICK %s\r\n" % nick, 'UTF-8'))
        except:
            print("Cannot Send Nick")

    def send_pass(self, password):
        try:
            self.con.send(bytes("PASS %s\r\n" % password, 'UTF-8'))
        except:
            print("Cannot Send Pass")

    def join_channel(self, chan):
        try:
            self.con.send(bytes("JOIN #%s\r\n" % chan, 'UTF-8'))
        except:
            print("Cannot Join")

    def part_channel(self, chan):
        self.con.send(bytes("PART %s\r\n" % chan, 'UTF-8'))

    def get_sender(self, msg):
        result = ""
        for char in msg:
            if char == "!":
                break
            if char != ":":
                result += char
        return result

    def get_message(self, msg):
        result = ""
        i = 3
        length = len(msg)
        while i < length:
            result += msg[i] + " "
            i += 1
        result = result.lstrip(':')
        return result

    def add_viewer(self, viewer):
        if viewer is not config.CHANNEL or viewer is not config.NICK:
            self.viewers_list[viewer] = user.User(viewer, self.cursor)

    def update_viewer_list(self, user_name):
        url = 'https://tmi.twitch.tv/group/user/' + user_name + '/chatters'
        try:
            info = json.loads(urllib.request.urlopen(url, timeout=15).read().decode())
            self.mods_list = info['chatters']['moderators']
            viewers = info['chatters']['viewers'] + info['chatters']['admins'] + info['chatters']['global_mods'] + info['chatters']['staff'] + info['chatters']['moderators']
            for viewer in viewers:
                if viewer not in self.viewers_list.keys():
                    self.add_viewer(viewer)
                else:
                    self.viewers_list[viewer].active = True
            self.conn.commit()
            keys_not_in_viewers = []
            for viewer in self.viewers_list.keys():
                if viewer not in viewers:
                    keys_not_in_viewers.append(viewer)
            for viewer in keys_not_in_viewers:
                self.viewers_list[viewer].active = False
        except BaseException as e:
            print("Get User Error: " + e)
            return False

    def award_user_time(self):
        for chatter in self.viewers_list:
            if self.viewers_list[chatter].active:
                self.viewers_list[chatter].add_time(self.cursor)

    def award_ticket(self):
        if self.awarding_tickets:
            for chatter in self.viewers_list:
                if self.viewers_list[chatter].active:
                    self.viewers_list[chatter].award_ticket(self.cursor)
        self.send_public_message("You are receiving raffle tickets for watching our stream!  You don't need to talk to get tickets, you just need to be here.  The drawing for our raffle will be at the end of the 48 hour stream on Sunday at 8pm EST.  We will randomly draw our winners and contact them via twtich.  Our 3 prizes this stream are $15, $20, and $60 steam game of your choosing!")

    def start_survey(self):
        self.awarding_tickets = True
        self.send_public_message("You are now receiving raffle tickets for watching our stream!  You don't need to talk to get tickets, you just need to be here.  The drawing for our raffle will be at the end of the 48 hour stream on Sunday at 8pm.  We will randomly draw our winners and contact them via twtich.  Our 3 prizes this stream are $15, $20, and $60 steam game of your choosing!")

    def end_survey(self):
        self.cursor.exectute("UDATE twitchstats_userstats SET raffle_tickets = 0")

    def process_command(self, message):
        if message is '!startsurvey':
            self.start_survey()
        if message is '!stopsurvey':
            self.end_survey()

    def receive_messages(self):
        try:
            self.data = self.data + self.con.recv(1024).decode('UTF-8')
            data_split = re.split(r"[~\r\n]+", self.data)
            self.data = data_split.pop()

            for line in data_split:
                line = str.rstrip(str(line))
                line = str.split(line)

                if len(line) >= 1:
                    if line[0] == 'PING':
                        self.send_pong(line[1])

                    else:
                        sender = self.get_sender(line[0])
                        message = self.get_message(line)
                        if "tmi.twitch.tv" not in sender:
                            if sender not in self.viewers_list.keys():
                                self.add_viewer(sender)
                            self.viewers_list[sender].add_message(self.cursor)
                            self.conn.commit()
                            if sender is config.CHANNEL:
                                self.process_commands(message)

        except socket.error:
            return False

        except socket.timeout:
            print("Socket timeout")
        except BaseException as e:
            print(e)




